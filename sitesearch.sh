#!/bin/bash
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# sitesearch.sh
# scrape a website and search for strings provided in a file
###
set -uf -o pipefail # no set -e. search engines can fail on strings

usage() {
	echo "Usage: `basename $0` [options]" 1>&2;
	echo ""
	echo "Search a list of strings on a website."
	echo "Uses googler and ddgr as search providers"
	echo ""
	echo "Options:"
	echo "-h 		Print this help."
	echo "-e ENGINE	Select search ENGINE"
	echo "   google	Use google"
	echo "   ddg		Use DuckDuckGo (default)"
	echo "-f FILE		Use FILE as list of search strings"
	echo "-s SITE		Target search SITE"
	exit 1;
}

# get search providers, if not yet  there
[[ -d ./ddgr ]] || [[ -x "$(command -v ddgr)" ]] || git clone https://github.com/jarun/ddgr.git
[[ -d ./googler ]] || [[ -x "$(command -v googler)" ]] || git clone https://github.com/jarun/googler.git

ENGINE="ddg"
FILE=""
SITE=""

while getopts ":he:s:f:" opt; do
	case ${opt} in
		e)	ENGINE=${OPTARG} ;
			[ $ENGINE == "google" -o $ENGINE == "ddg" ] || (echo "ERROR: ${ENGINE} is not a valid search engine"; usage)
			;;
		s)	SITE=${OPTARG}
			;;
		f)	FILE=${OPTARG}
			[ -f ${FILE} ] || (echo "ERROR: ${FILE} not found"; usage)
			;;
		h | *)	usage
			;;

	esac
done
shift $((OPTIND -1))

# set search provider
if [ $ENGINE == "ddg" ]; then
	ENGINE=./ddgr/ddgr
fi

if [ $ENGINE == "google" ]; then
	ENGINE=./googler/googler
fi

# check for file and site
if [ -z "$FILE" ]; then echo "ERROR: No file provided. Use -f."; usage; fi
if [ -z "$SITE" ]; then echo "ERROR: No site provided. Use -s."; usage; fi

# init arrays
declare -a FOUND=();
declare -a NOTFOUND=();

# exec search
for i in `cat ${FILE}`; do
	echo "${ENGINE} --np \"${i}\" site:\"${SITE}\" | grep \"${i}\"";
	result=`${ENGINE} --np "\"${i}\" site:\"${SITE}\"" | grep "${i}"`
	if [ -z "$result" ]; then
		NOTFOUND+=("$i");
		echo "$i failed";
	else
		FOUND+=("$i");
		echo "$i success";
	fi
	snooze=$(((RANDOM %30)+30));
	echo "sleep $snooze";
	sleep $snooze;
done

# print results
echo "Success finding email adresses:"
for i in ${FOUND[@]}; do echo "$i"; done
echo

echo "Failed finding the following email adresses:"
for i in ${NOTFOUND[@]}; do echo "$i"; done
