# sitesearch

Script to search a website for a list of strings.

## Requirements

Uses [ddgr](https://github.com/jarun/ddgr.git) and [googler](https://github.com/jarun/googler.git) as search providers.
If not available the script automatically downloads the projects.

## Usage

```
Usage: sitesearch.sh [options]

Search a list of strings on a website.
Uses googler and ddgr as search providers

Options:
-h 		Print this help.
-e ENGINE	Select search ENGINE
   google	Use google
   ddg		Use DuckDuckGo (default)
-f FILE		Use FILE as list of search strings
-s SITE		Target search SITE
```


